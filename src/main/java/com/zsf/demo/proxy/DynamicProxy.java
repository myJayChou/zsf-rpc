package com.zsf.demo.proxy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.zsf.demo.client.ChannelHandlerManager;
import com.zsf.demo.client.NettyClientHandler;
import com.zsf.demo.future.RpcReqResponseFuture;
import com.zsf.demo.pojo.RpcRequest;
import com.zsf.demo.pojo.RpcResponse;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * @author tangbu
 */
public class DynamicProxy implements InvocationHandler {


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws ExecutionException, InterruptedException, JsonProcessingException {
        if ("toString".equals(method.getName())){
            return proxy.toString();
        }
        RpcRequest request = new RpcRequest();
        request.setRequestId(UUID.randomUUID().toString());
        request.setClassName(method.getDeclaringClass().getName());
        request.setMethodName(method.getName());
        request.setParameterTypes(method.getParameterTypes());
        request.setParameters(args);
        request.setVersion(1);

        System.out.println("动态代理封装的request"+ request);

        System.out.println("----------------执行远程调用--------");

        RpcResponse response = invokeRpc(request);

        System.out.println("远程调用返回的结果"+ response);

        return response.getResult();
    }


    int count = 0;
    private RpcResponse invokeRpc(RpcRequest request) throws ExecutionException, InterruptedException, JsonProcessingException {
        count++;
        //如果server不存在 返回值为空 不处理会报错
        NettyClientHandler nettyClientHandler = ChannelHandlerManager.chooseHandler("127.0.0.1", count % 2 == 0 ? 8888 : 8889);
        if (nettyClientHandler==null){
            //不存在抛出异常
            throw new RuntimeException("server not exist");
        }
        RpcReqResponseFuture rpcReqResponseFuture = nettyClientHandler.sendRpcRequest(request);

        return rpcReqResponseFuture.get();
    }
}
