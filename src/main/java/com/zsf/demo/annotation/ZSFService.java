package com.zsf.demo.annotation;

import java.lang.annotation.*;

/**
 * @author hengye
 * @version 1.0
 * @Description
 * @date 2024/7/26 15:40
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Inherited
public @interface ZSFService {
    String value() default "";
    String version() default "";
    String group() default "";
    String timeout() default "";
    String retries() default "";
    String loadBalance() default "";

}
