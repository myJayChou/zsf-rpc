package com.zsf.demo.server;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zsf.demo.exception.DkRuntimeException;
import com.zsf.demo.pojo.RpcRequest;
import com.zsf.demo.pojo.RpcResponse;
import com.zsf.demo.struct.NettyMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;


/**
 * @author tangbu
 */
public class NettyServerHandler extends ChannelInboundHandlerAdapter {
    public static Map<String,Object> serviceMap = new HashMap<>();
    private ObjectMapper mapper = new ObjectMapper();


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        NettyMessage message = (NettyMessage) msg;
        System.out.println("服务端收到的消息是:" + message);
        JsonNode rpcRequestBody = message.getBody();
        // 反序列化 得到Rpc请求对象
        RpcRequest rpcRequest = mapper.readValue(rpcRequestBody.toString(), RpcRequest.class);
        //根据rpc请求对象调用对应的方法 返回响应
        RpcResponse rpcResponse = handleRpcRequest(rpcRequest);

        NettyMessage response = new NettyMessage();
        response.setType((byte) 1);
        response.setPriority((byte) 2);
        response.setBody(mapper.readValue(mapper.writeValueAsString(rpcResponse), JsonNode.class));

        ctx.channel().writeAndFlush(response);

    }

    private RpcResponse handleRpcRequest(RpcRequest rpcRequest) {
        String requestId = rpcRequest.getRequestId();
        RpcResponse response = new RpcResponse();
        response.setRequestId(requestId);
        try {
            String className = rpcRequest.getClassName();
            String methodName = rpcRequest.getMethodName();
            Class<?>[] parameterTypes = rpcRequest.getParameterTypes();
            Object[] parameters = rpcRequest.getParameters();

            Object o = serviceMap.get(className);
            if (o == null){
                throw new DkRuntimeException("服务不存在");
            }
            //通过反射调用方法返回结果
            Class clazz = Class.forName(className);
            Method method = clazz.getMethod(methodName, parameterTypes);
            Object result = method.invoke(o, parameters);
            response.setSuccess(true);
            response.setResult(result);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage(e.getMessage());
        }

        return response;
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
        ctx.fireExceptionCaught(cause);

    }
}
