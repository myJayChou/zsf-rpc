package com.zsf.demo.exception;

/**
 * @author hengye
 * @version 1.0
 * @Description
 * @date 2024/7/26 14:25
 */
public class DkException extends Exception{
    public DkException(String message)
    {
        super(message);
    }

    public DkException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DkException(Throwable cause)
    {
        super(cause);
    }


}
