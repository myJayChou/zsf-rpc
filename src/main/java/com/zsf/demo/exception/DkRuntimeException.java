package com.zsf.demo.exception;

/**
 * @author hengye
 * @version 1.0
 * @Description
 * @date 2024/7/26 14:27
 */
public class DkRuntimeException extends Exception{
    public DkRuntimeException(String message)
    {
        super(message);
    }
    public DkRuntimeException(String message, Throwable cause)
    {
        super(message, cause);
    }
    public DkRuntimeException(Throwable cause)
    {
        super(cause);
    }
}
