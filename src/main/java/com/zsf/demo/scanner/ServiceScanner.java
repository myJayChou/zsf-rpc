package com.zsf.demo.scanner;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hengye
 * @version 1.0
 * @Description 服务扫描器
 * @date 2024/7/26 16:04
 */
public class ServiceScanner {



        public static List<Class<?>> findAnnotatedClasses(String packageName) {
            List<Class<?>> annotatedClasses = new ArrayList<>();
            // 这里需要添加注解的完整类名
            String annotationName = "ZSFService";
            String packagePrefix = "com.zsf.demo.service";
            String package2Prefix = "com.zsf.demo.annotation";
            try {
                // 获取包名对应的URL
                URL url = new URL("file", null, packageName.replace('.', '/'));
                // 从URL获取文件列表
                File directory = new File(url.getFile());

                // 遍历文件
                for (File file : directory.listFiles() ){
                    // 移除.class后缀并转换为全类名
                    System.out.println(file);
                    String className = file.getName().substring(0, file.getName().length() - 5);
                    System.out.println(className);
                    Class<?> clazz = Class.forName(packagePrefix.concat(".").concat(className));
                    // 检查类是否带有指定注解
                    if (clazz.isAnnotationPresent((Class) Class.forName(package2Prefix.concat(".").concat(annotationName)))) {
                        annotatedClasses.add(clazz);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return annotatedClasses;
        }

        public static void main(String[] args) {
            List<Class<?>> annotatedClasses = findAnnotatedClasses("/Users/znly/IdeaProjects/myrpc/src/main/java/com/zsf/demo/service");
            for (Class<?> clazz : annotatedClasses) {
                System.out.println("存在的:"+clazz.getName());
                //获取名称
                String[] split = clazz.getName().split("\\.");
                System.out.println(split[split.length-1]);
                //新建一个实例
                System.out.println(clazz);
            }

        }

}
