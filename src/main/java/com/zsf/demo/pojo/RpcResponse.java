package com.zsf.demo.pojo;


import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author tangbu
 */
public class RpcResponse  {

    private String requestId;
    private boolean success;
    private String message;
    private Object result;
    private int version;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("requestId", requestId)
                .append("success", success)
                .append("message", message)
                .append("result", result)
                .append("version", version)
                .build();

    }
}
