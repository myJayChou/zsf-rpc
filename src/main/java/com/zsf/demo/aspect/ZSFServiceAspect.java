package com.zsf.demo.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

/**
 * @author hengye
 * @version 1.0
 * @Description
 * @date 2024/7/26 15:42
 */
@Aspect

public class ZSFServiceAspect {
    //定义切入点为标记@ZSFService注解的方法
    @Pointcut(value = "this(com.zsf.demo.annotation.ZSFService)")
    public void ZSFServiceAspect(){}

    @Around("ZSFServiceAspect")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        // 获取类名
        String className = joinPoint.getSignature().getDeclaringTypeName();
        //创建一个对象实例
        Class<?> aClass = Class.forName(className);
        System.out.println(aClass);
        System.out.println("Class name: " + className);

        return joinPoint.proceed();
    }

}
