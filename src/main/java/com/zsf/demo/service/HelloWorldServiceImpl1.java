package com.zsf.demo.service;


import com.zsf.demo.annotation.ZSFService;

@ZSFService
public class HelloWorldServiceImpl1 implements HelloWorldService {
    @Override
    public String helloWorld(String name) {
        return "Hello World1" + name;
    }
}
