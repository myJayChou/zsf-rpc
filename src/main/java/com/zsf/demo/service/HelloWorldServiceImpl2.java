package com.zsf.demo.service;

import com.zsf.demo.annotation.ZSFService;

@ZSFService
public class HelloWorldServiceImpl2 implements HelloWorldService {
    @Override
    public String helloWorld(String name) {
        return "Hello World2" + name;
    }
}
