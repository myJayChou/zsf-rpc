package com.zsf.demo.service;

/**
 * @author tangbu
 */
public interface HelloWorldService {

    String helloWorld(String name);

}
