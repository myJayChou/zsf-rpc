## 概要
RPC（Remote Procedure Call）是指远程过程调用,是微服务时代必不可少的技术。

常见的RPC框架有
* Apache Dubbo
* Google gRPC
* Apache Thrift
* Spring Cloud的Feign

本次主要通过netty网络通信框架简易实现一个RPC框架。




